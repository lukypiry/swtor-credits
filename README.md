Players on this type of server are interested in interacting with other players with a more immersive, lore-focused kind of thinking. They still want to participate in other features of the game, but for them, the journeys and paths to develop characters, how to interact with others, and quest material are what I have. Most of the time they are found in other areas that offer this style of server at [https://www.goldseasy.com/](https://www.goldseasy.com/).

BioWare seems to want to attract people who want to act in role-playing games and "personality" by specifying this type of server. Those who want to live the life of their character so that it can be applied to the folklore of the territory.

We still don't know how much control they plan to implement this kind of server farm, but from the customer support staff, unless the people committing and the immersive attacks are extreme or regular, you can't expect too much action. This item I already feel that there are more situations that are felt when they provide servers labeled like this. That way people with that mindset will have a place to go and experience the game however they please with like-minded players.

In war zones and open world PVP areas, such as "lakes", you still have the option to do PVP interactions as needed. You must manually select the character option option to dial in PvP combat. Otherwise, you will not be able to attack or attack other players unless you are in an area specially designed for PVP, such as the planet Ilum. I am using the / duel command.

RP-PvP (role player vs player)
In this type of server environment, players are encouraged to pursue their desires and role-play and "character" while engaging in PvP-style combat. It's not based on player-vs-player blues or constant testing of skill, or taking the top spot on the leaderboard, but world folklore and how it relates to PvP. Useful for the PvP styles you focus on.

A person who recognizes and appreciates the fact that there is a battle going on between the empire and the republic during the Star Wars Old Republic era. War between the two is inevitable, and the desire to play your character in this way of thinking and immerse yourself in it will find this style of server to your best taste.

BioWare seems to want to attract people who want to role-play and "character" while doing PVP by specifying this type of server. A person who wants to live a life of character that fits with the tradition of the territory, the history of the times and the environment.

We still don't know how much control they plan to implement this kind of server farm, but from the customer support staff, unless the people committing and the immersive attacks are extreme or regular, you can't expect too much action. This item I already feel that there are more situations that are felt when they provide servers labeled like this. That way people with that mindset will have a place to go and experience the game however they please with like-minded players.

The PvP feature for this type of server is "always on" and cannot be disabled. Unless you're in an area designed as a "safe place" not attacked, like the home world, faction fleet, or capital world, anyone who wants to die is fair game. I want to do that with a method of type "on character".

So if you're itching for the thrill of a surprise attack out of the blue, or if you want to act like a hunter looking for the thrill of the chase, in your normal favorite playstyle, if you want to talk to others and act on the of yours. This type is the best option for you, as if the character is actually part of a real lore and not a PVP character on the SWTOR server.